/**
 * @file
 * Provides the Facebook sdk.
 */

(function ($, d, s, id) {
  'use strict';

  /**
   * Add the Facebook SDK.
   */
  Drupal.behaviors.facebookPageInteraction = {
    attach: function (context, settings) {
      var js = d.getElementsByTagName(s)[0];
      var fjs = d.getElementsByTagName(s)[0];

      if (d.getElementById(id)) {
        return;
      }

      js = d.createElement(s); js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7';
      fjs.parentNode.insertBefore(js, fjs);
    }
  };
})(jQuery, document, 'script', 'facebook-jssdk');
