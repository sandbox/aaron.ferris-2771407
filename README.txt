-- SUMMARY --
This module provides a block that displays a Facebook page, events, messaging functionality, timeline, share, like etc. The module offers the same customisation currently available via the Facebook Page API, see here for further information; https://developers.facebook.com/docs/plugins/page-plugin/

-- REQUIREMENTS --
None

-- INSTRUCTIONS --

1. Enable the module.
2. Configure permissions
3. Add your Facebook page URL in the module settings
4. Configure the module settings
5. Add the Facebook page interaction block where required