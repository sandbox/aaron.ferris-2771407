<?php
/**
 * @file
 * facebook_page_interaction admin form.
 */

/**
 * Facebook page interaction admin settings.
 */
function facebook_page_interaction_admin_form($form, &$form_state) {
  $form['facebook_page_interaction'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook page interaction settings'),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_url'] = array(
    '#title' => t('Facebook page URL'),
    '#type' => 'textfield',
    '#description' => t('The URL of the Facebook page.'),
    '#default_value' => variable_get('facebook_page_interaction_url', ''),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_messages_enabled'] = array(
    '#title' => t('Display messages tab'),
    '#type' => 'checkbox',
    '#description' => t('Turn off/on the messages tab.'),
    '#default_value' => variable_get('facebook_page_interaction_messages_enabled', TRUE),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_messages_enabled_help'] = array(
    '#type' => 'markup',
    '#markup' => '<p>To <strong>enable messaging</strong> on your Facebook page go to your Page <strong>Settings</strong>. In the row <strong<Messages</strong> check "Allow people to contact my Page privately" by showing the Message button (Direct Link: https://www.facebook.com/{your-page-name}/settings/?tab=settings&section=messages&view).</p>',
  );

  $form['facebook_page_interaction']['facebook_page_interaction_events_enabled'] = array(
    '#title' => t('Display events tab'),
    '#type' => 'checkbox',
    '#description' => t('Turn off/on the events tab.'),
    '#default_value' => variable_get('facebook_page_interaction_events_enabled', TRUE),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_timeline_enabled'] = array(
    '#title' => t('Display timeline tab'),
    '#type' => 'checkbox',
    '#description' => t('Turn off/on the timeline tab.'),
    '#default_value' => variable_get('facebook_page_interaction_timeline_enabled', TRUE),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_display_faces'] = array(
    '#title' => t('Display Facebook user photos'),
    '#type' => 'checkbox',
    '#description' => t('Show profile photos when friends like this.'),
    '#default_value' => variable_get('facebook_page_interaction_display_faces', TRUE),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_hide_cover'] = array(
    '#title' => t('Hide Facebook page cover photo'),
    '#type' => 'checkbox',
    '#description' => t('Hide cover photo in the header.'),
    '#default_value' => variable_get('facebook_page_interaction_hide_cover', FALSE),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_small_header'] = array(
    '#title' => t('Display smaller header'),
    '#type' => 'checkbox',
    '#description' => t('Use the small header instead. <strong>Note: this will disable the "share" button</strong>'),
    '#default_value' => variable_get('facebook_page_interaction_small_header', FALSE),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_width'] = array(
    '#title' => t('Plugin width'),
    '#type' => 'textfield',
    '#description' => t('The pixel width of the plugin. Min. is 180 & Max. is 500. Leave empty to auto fill available space.'),
    '#default_value' => variable_get('facebook_page_interaction_width', ''),
  );

  $form['facebook_page_interaction']['facebook_page_interaction_height'] = array(
    '#title' => t('Plugin height'),
    '#type' => 'textfield',
    '#description' => t('The pixel height of the plugin. Min. is 70.'),
    '#default_value' => variable_get('facebook_page_interaction_height', ''),
  );

  $form['#validate'][] = 'facebook_page_interaction_admin_form_validate';

  return system_settings_form($form);
}

/**
 * Validate Facebook page interaction admin settings.
 */
function facebook_page_interaction_admin_form_validate($form, &$form_state) {
  if(!empty($form_state['values']['facebook_page_interaction_width'])) {
    $width = $form_state['values']['facebook_page_interaction_width'];
  }

  if(!empty($form_state['values']['facebook_page_interaction_height'])) {
    $height = $form_state['values']['facebook_page_interaction_height'];
  }

  // The plugin width must be equal to, or greater than 180px and no more than 500px.
  if (!empty($width) && ($width < 180 || $width > 500)) {
    form_set_error('facebook_page_interaction_width', t('The pixel width of the plugin must be minimum 180px and maximum 500px'));
  }

  // The plugin height must be equal to, or greater than 180px
  if (!empty($height) && $height < 70) {
    form_set_error('facebook_page_interaction_height', t('The pixel height of the plugin must be minimum 70px'));
  }
}
